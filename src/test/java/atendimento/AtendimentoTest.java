package atendimento;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.springframework.test.context.junit4.SpringRunner;

import br.com.atendimento.dto.PrestadorDto;
import br.com.atendimento.enumeration.Especialidade;
import br.com.atendimento.service.AtendimentoService;
import br.com.atendimento.util.Util;

@RunWith(SpringRunner.class)
public class AtendimentoTest {
	
	@InjectMocks
	private AtendimentoService atendimentoService;
	
	@Test
	public void mostraTodosAtendimento() {		
		List<PrestadorDto> lstPrestador = Util.lstPrestador();
		assertThat(lstPrestador, is(atendimentoService.obterPrestaresSaude(-100.00, -2000.00, Especialidade.CARDIOLOGISTA.getNome())));
	}
	
	
	@Test
	public void cnpjNotFound() {		
		assertEquals("ERROR", (atendimentoService.buscaCnpjReceita("019558080001951").getStatus()));
	}

}
