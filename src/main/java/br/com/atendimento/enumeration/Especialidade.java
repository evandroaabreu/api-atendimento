package br.com.atendimento.enumeration;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum Especialidade {
	
	CARDIOLOGISTA("Cardiologita"), DERMATOLOGIA("Dermatolgia"), OFTALMOLOGIA("Oftalmologia");
	
	private String nome;

}
