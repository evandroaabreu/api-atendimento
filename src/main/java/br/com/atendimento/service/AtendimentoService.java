package br.com.atendimento.service;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import br.com.atendimento.dto.EmpresaCnpjDTO;
import br.com.atendimento.dto.PrestadorDto;
import br.com.atendimento.util.Util;

@Service
public class AtendimentoService {

	public List<PrestadorDto> obterPrestaresSaude(Double latitude, Double longitude, String especialidade) {
		return Util.lstPrestador();

	}

	public EmpresaCnpjDTO buscaCnpjReceita(String cnpj) {
		RestTemplate restTemplate = new RestTemplate();
		cnpj = cnpj.replaceAll("[^0-9]", "");
		EmpresaCnpjDTO emp = null;

		try {
			emp = restTemplate.getForObject("https://www.receitaws.com.br/v1/cnpj/" + cnpj, EmpresaCnpjDTO.class);
		} catch (Exception e) {
			emp = new EmpresaCnpjDTO();
			emp.setStatus("ERROR");
			emp.setMessage(e.getMessage());
		}

		return emp;

	}

}
