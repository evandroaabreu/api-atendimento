package br.com.atendimento.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.atendimento.dto.EmpresaCnpjDTO;
import br.com.atendimento.dto.PrestadorDto;
import br.com.atendimento.service.AtendimentoService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/api-atendimento")
@Api(value = "API Atendimento")
public class AtendimentoController {

	@Autowired
	private AtendimentoService atendimentoService;

	@GetMapping("/obterPrestaresSaude/{latitude}/{longitude}/{especialidade}")
	@ApiOperation(value = "api obter prestares de saude", response = String.class)
	public ResponseEntity<List<PrestadorDto>> obterPrestaresSaude(@PathVariable("latitude") Double latitude,
			@PathVariable("longitude") Double longitude, @PathVariable("especialidade") String especialidade) {
		return ResponseEntity.ok(atendimentoService.obterPrestaresSaude(latitude, longitude, especialidade));
	}

	@GetMapping("/obterEmpresa/{cnpj}")
	@ApiOperation(value = "api obter prestares de saude", response = String.class)
	public ResponseEntity<EmpresaCnpjDTO> obterEmpresa(@PathVariable("cnpj") String cnpj) {
		return ResponseEntity.ok(atendimentoService.buscaCnpjReceita(cnpj));
	}

}
