package br.com.atendimento.util;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import br.com.atendimento.comparator.AtendimentoComparator;
import br.com.atendimento.dto.EspecialidadeDto;
import br.com.atendimento.dto.PrestadorDto;
import br.com.atendimento.enumeration.Especialidade;

public class Util {
	
	public static List<PrestadorDto> lstPrestador(){
		List<PrestadorDto> lst = new ArrayList<PrestadorDto>();
		lst.add(adicionaPrestador1());
		lst.add(adicionaPrestador2());
		lst.add(adicionaPrestador3());	
		Collections.sort(lst,new AtendimentoComparator());
		return lst;
	}

	private static PrestadorDto adicionaPrestador1() {
		return PrestadorDto.builder().id(1).nome("Luiz").endereco("Rua 13 de maio").latitude(-10000.00).logitude(-15000.00)
				.distanciaEmKm(5000.00)
				.lstEspecialidade(adicionaEspecialidadePrestador1())
				.build();
	}

	private static List<EspecialidadeDto> adicionaEspecialidadePrestador1() {
		List<EspecialidadeDto> lst = new ArrayList<EspecialidadeDto>();		
		EspecialidadeDto.builder().id(1).descricao(Especialidade.CARDIOLOGISTA.getNome()).build();
		lst.add(EspecialidadeDto.builder().id(2).descricao(Especialidade.CARDIOLOGISTA.getNome()).build());
		lst.add(EspecialidadeDto.builder().id(3).descricao(Especialidade.DERMATOLOGIA.getNome()).build());
		return lst;
	}
	
	
	private static PrestadorDto adicionaPrestador2() {
		return PrestadorDto.builder().id(2).nome("Marcos").endereco("Rua 14 de maio").latitude(-11100.00).logitude(-16000.00)
				.distanciaEmKm(3000.00)
				.lstEspecialidade(adicionaEspecialidadePrestador2())
				.build();
	}
	
	private static List<EspecialidadeDto> adicionaEspecialidadePrestador2() {
		List<EspecialidadeDto> lst = new ArrayList<EspecialidadeDto>();		
		lst.add(EspecialidadeDto.builder().id(2).descricao(Especialidade.CARDIOLOGISTA.getNome()).build());
		return lst;
	}


	
	private static PrestadorDto adicionaPrestador3() {
		return PrestadorDto.builder().id(3).nome("Patricia").endereco("Rua 15 de maio").latitude(-11200.00).logitude(-17000.00)
				.distanciaEmKm(6000.00)
				.lstEspecialidade(adicionaEspecialidadePrestador3())
				.build();
	}
	
	private static List<EspecialidadeDto> adicionaEspecialidadePrestador3() {
		List<EspecialidadeDto> lst = new ArrayList<EspecialidadeDto>();		
		lst.add(EspecialidadeDto.builder().id(3).descricao(Especialidade.DERMATOLOGIA.getNome()).build());
		return lst;
	}
	
}
