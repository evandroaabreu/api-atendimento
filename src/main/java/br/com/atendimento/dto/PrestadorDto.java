package br.com.atendimento.dto;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class PrestadorDto {
	
	private Integer id;
	private String nome;
	private String endereco;
	private Double latitude;
	private Double logitude;
	private Double distanciaEmKm;
	List<EspecialidadeDto> lstEspecialidade;
	

}
