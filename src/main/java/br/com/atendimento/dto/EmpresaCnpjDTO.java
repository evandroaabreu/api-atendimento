package br.com.atendimento.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class EmpresaCnpjDTO {

	private String cnpj;

	private String nome;
	private String fantasia;

	private String endereco;

	private String logradouro;
	private String numero;
	private String complemento;
	private String cep;
	private String bairro;

	private String municipio;
	private String uf;

	private Integer cidadeId;
	private Integer estadoId;

	private String status;
	private String message;

}
