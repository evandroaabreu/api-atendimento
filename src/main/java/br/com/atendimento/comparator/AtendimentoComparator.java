package br.com.atendimento.comparator;

import java.util.Comparator;

import br.com.atendimento.dto.PrestadorDto;

public class AtendimentoComparator implements Comparator<PrestadorDto>{

	@Override
	public int compare(PrestadorDto prestadorDto1, PrestadorDto prestadorDto2) {
		 if(prestadorDto1.getDistanciaEmKm() > prestadorDto2.getDistanciaEmKm()){
	            return 1;
	        } else {
	            return -1;
	        }
	}

}
